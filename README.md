# learn-GitLab-flow-with-release-3

- [x] edit readme (edit-readme)
- [ ] downsteam: stable release 1.0 (r-1.0)
- [x] feature-2.0 (f-2.0)
- [ ] downsteam: stable release 2.0 (r-2.0)
- [ ] bugfix-1.1 (r-1.1)
- [ ] feature-3.0 (f-3.0)
- [ ] downsteam: stable release 3.0 (r-3.0)